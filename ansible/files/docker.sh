#! /bin/bash
export PATH=$PATH:/usr/local/bin/
usermod -aG docker ec2-user
usermod -aG docker jenkins
newgrp docker
mkdir /home/jenkins/jenkins_compose
cat >>docker-compose.yaml <<'END'
# docker-compose.yaml
version: '3.8'
services:
  jenkins:
    image: jenkins/jenkins:lts
    privileged: true
    user: jenkins
    ports:
     - 8080:8080
     - 50000:50000
  container_name: jenkins
  volumes:
    - /home/jenkins/jenkins_compose/jenkins_configuration:/var/jenkins_home
    - /var/run/docker.sock:/var/run/docker.sock
END
docker-compose up -d
