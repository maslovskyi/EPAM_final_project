#!/usr/bin/env bash

pip3 install boto3 botocore
yum install python-boto3 -y
yum install python-botocore -y
ansible-galaxy collection install community.docker
ansible-galaxy collection install amazon.aws
ansible-playbook playbook.yml -i inventories/nodes/hosts.aws_ec2.yml
# ansible-galaxy install -r requirements.yml -p ./external_roles
# ansible-playbook playbook.yml -i inventories/nodes/hosts.aws_ec2.yml -b --ask-vault-pass
