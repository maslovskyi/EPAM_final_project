
variable "availability_zone" {
  type        = string
  default     = "eu-west-2a"
  description = "london"
}

variable "ami" {
  type        = string
  default     = "ami-0648ea225c13e0729"
  description = "t3_micro amazon linux"
}

# using ssh key
variable "key_name" {
  type        = string
  default     = "terraform"
  description = ""
}

variable "vpc_cidr" {
  type        = string
  description = ""
  default     = "192.168.10.0/24"
}

variable "cidr" {
  type        = string
  description = ""
  default     = "192.168.10.0/24"
}

variable "nodes_type" {
  type        = string
  default     = "t3.micro"
  description = "the newest gens"
}

variable "admin_ip_addresses" {
  type = list(string)
  default = [
    "192.168.10.0/24",
    "217.20.178.137/32" # worker network for runner
  ]
}

variable "environment" {
  type    = string
  default = "infrastructure"
}

variable "region" {
  type        = string
  default     = "eu-west-2"
  description = "UK-London"
}
