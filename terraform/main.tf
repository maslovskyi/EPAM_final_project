###----- An infrastructure deploy for Ansible project -----###

# declaring instanses
resource "aws_instance" "node1" {

  ami                         = var.ami
  instance_type               = var.nodes_type
  key_name                    = var.key_name
  associate_public_ip_address = true
  availability_zone           = var.availability_zone
  subnet_id                   = aws_subnet.infrastructure-subnet.id
  vpc_security_group_ids      = [aws_security_group.nodes-inbound.id]
  root_block_device {
    delete_on_termination = true
    volume_size           = "8"
    volume_type           = "gp3"
  }
  tags = {
    Name        = "node1"
    env         = "infrastructure"
    Description = "First Ansible worker"
  }
}

resource "aws_instance" "node2" {

  ami                         = var.ami
  instance_type               = var.nodes_type
  key_name                    = var.key_name
  associate_public_ip_address = true
  availability_zone           = var.availability_zone
  subnet_id                   = aws_subnet.infrastructure-subnet.id
  vpc_security_group_ids      = [aws_security_group.nodes-inbound.id]
  root_block_device {
    delete_on_termination = true
    volume_size           = "8"
    volume_type           = "gp3"
  }
  tags = {
    Name        = "node2"
    env         = "infrastructure"
    Description = "Second Ansible worker"
  }
}
resource "aws_instance" "runner" {

  ami                         = var.ami
  instance_type               = var.nodes_type
  key_name                    = var.key_name
  associate_public_ip_address = true
  availability_zone           = var.availability_zone
  subnet_id                   = aws_subnet.infrastructure-subnet.id
  vpc_security_group_ids      = [aws_security_group.nodes-inbound.id]
  root_block_device {
    delete_on_termination = true
    volume_size           = "8"
    volume_type           = "gp3"
  }
  tags = {
    Name        = "runner"
    env         = "infrastructure"
    Description = "Main node for Ansible work"
  }
}
# creating infrastructure vpc
resource "aws_vpc" "infrastructure-vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "infrastructure"
    env  = "infrastructure"
  }
}

resource "aws_subnet" "infrastructure-subnet" {
  vpc_id            = aws_vpc.infrastructure-vpc.id
  cidr_block        = var.cidr
  availability_zone = var.availability_zone
  tags = {
    Name = "infrastructure"
    env  = "infrastructure"
  }
}

# creating a security group for nods
resource "aws_security_group" "nodes-inbound" {
  name   = "infrastructure"
  vpc_id = aws_vpc.infrastructure-vpc.id
  tags = {
    Name = "infrastructure"
    env  = "infrastructure"
  }
}

# admin access
resource "aws_security_group_rule" "admin-access" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  security_group_id = aws_security_group.nodes-inbound.id
  cidr_blocks       = var.admin_ip_addresses
}

# security group for outgoing traffic
resource "aws_security_group_rule" "outbound_all" {
  type              = "egress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "all"
  security_group_id = aws_security_group.nodes-inbound.id
  cidr_blocks       = ["0.0.0.0/0"]
}

# creating an internet gateway for nods vpc
resource "aws_internet_gateway" "igw-infrastructure" {
  vpc_id = aws_vpc.infrastructure-vpc.id
  tags = {
    Name = "infrastructure"
    env  = "infrastructure"
  }
}

# creating a route table
resource "aws_route" "route" {
  route_table_id         = aws_vpc.infrastructure-vpc.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw-infrastructure.id
}

# outputs for nods
output "publicip_node1" {
  value = aws_instance.node1.public_ip
}
output "publicip_node2" {
  value = aws_instance.node2.public_ip
}
output "publicip_runner" {
  value = aws_instance.runner.public_ip
}
