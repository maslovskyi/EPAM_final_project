# Animal shelter project 

For the final project I choose a stack of terraform, ansible, Jenkins.

## Installation

First of all you need to create an infrastructure using terraform from tf directory ./terraform


## Next step to apply sensible playbook & install Jenkins

From ./ansible/files/ run ansible.sh , then on runner node run docker.sh for install Jenkins. Use requrements.sh for install dependency's on instance where you will run ansible playbook.

## Make pipeline in Jenkins 

Exists jobs represents in ./jenkins directory with animal_shelter_make_docker_image & Run_and_test_docker_image. In ./jenkins/images screens of Jenkins jobs and nodes.

## Homepage

Some preview [Animal Shelter](http://35.178.149.22:80)